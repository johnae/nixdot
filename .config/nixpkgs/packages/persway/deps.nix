[

  {
    goPackagePath = "github.com/johnae/go-i3";
    fetch = {
      type = "git";
      url = "https://github.com/johnae/go-i3";
      rev = "ec26583ede159ef14933ae71410ab121aa6e55b6";
      sha256 = "01cwv13vnbidlff094z2n7ij1kx2s45b5qbzgx4fr91rvahqgbnv";
    };
  }
  {
    goPackagePath = "github.com/BurntSushi/xgbutil";
    fetch = {
      type = "git";
      url = "https://github.com/BurntSushi/xgbutil";
      rev = "f7c97cef3b4e6c88280a5a7091c3314e815ca243";
      sha256 = "0kw0zmpbnymnalpj2wsvxbb7p5c297kv0qhyj20mcwg8dz64drnp";
    };
  }
  {
    goPackagePath = "github.com/BurntSushi/xgb";
    fetch = {
      type = "git";
      url = "https://github.com/BurntSushi/xgb";
      rev = "27f122750802c950b2c869a5b63dafcf590ced95";
      sha256 = "18lp2x8f5bljvlz0r7xn744f0c9rywjsb9ifiszqqdcpwhsa0kvj";
    };
  }
]
